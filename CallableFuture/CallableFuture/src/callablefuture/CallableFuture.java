/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callablefuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author avilapm
 */
public class CallableFuture {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

        long startTime = System.nanoTime();
        
        Future<Double> future0 = executor.submit(() -> {
            double sum = 0;
            for (int i = 1; i < 2E7; i++) {
                sum += i;
                Math.sin(sum);
                        
            }
            return sum;
        });

        Future<Double> future1 = executor.submit(() -> {
            double sum = 0;
            for (int i = 1; i < 2E7; i++) {
                sum += i+2E7;
                Math.sin(sum);
                        
            }
            return sum;
        });
        
        Future<Double> future2 = executor.submit(() -> {
            double sum = 0;
            for (int i = 1; i < 2E7; i++) {
                sum += i+4E7;
                Math.sin(sum);
                        
            }
            return sum;
        });
        
        Future<Double> future3 = executor.submit(() -> {
            double sum = 0;
            for (int i = 1; i < 2E7; i++) {
                sum += i+6E7;
                Math.sin(sum);
                        
            }
            return sum;
        });
        
        Future<Double> future4 = executor.submit(() -> {
            double sum = 0;
            
            for (int i = 1; i <= 2E7; i++) {
                sum += i+8E7;
                Math.sin(sum);
            }
            return sum;
        });

        try {
            System.out.println("Sum = " + (future0.get() + future1.get() + future2.get() + future3.get() + future4.get()));
            long endTime = System.nanoTime();
            long totalTime = endTime - startTime;
            System.out.println(totalTime/1E9);
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(CallableFuture.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            executor.shutdown();
        }

    }

}
